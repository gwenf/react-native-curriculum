# react-native-curriculum
[link to freeCodeCamp issue where this curriculum is being discussed](https://github.com/freeCodeCamp/freeCodeCamp/issues/14595)

React Native challenge list for freeCodeCamp.

1. Hello World
1. `View` and `Text`
1. Props (2 challenges)
1. State
1. Styling (2 challenges - 1 for inline and one for separate styles object)
1. Dimensions (2 challenges)
1. Layouts with Flexbox (3 challenges)
1. Input Fields - Text
1. Touchables (2-3 challenges)
1. ScrollView
1. ListView (2 challenges)
1. Networking with Fetch (1-2 challenges)
1. Networking with Websockets
1. Navigator.geolocation API
1. react-native link

Tutorials for how to start running and building locally...

## Related Libraries:

1. [Vector Icons](https://github.com/oblador/react-native-vector-icons)
1. [Google Maps](https://github.com/airbnb/react-native-maps)
1. [Navigation](https://github.com/wix/react-native-navigation)

## Tools:

1. [Expo Web](https://snack.expo.io/)

## Some Links:

* Testing with Enzyme - [Enzyme Docs](http://airbnb.io/enzyme/docs/guides/react-native.html)
* [React Native Docs Tutorial](https://facebook.github.io/react-native/docs/tutorial.html)
